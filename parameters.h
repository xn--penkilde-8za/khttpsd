#ifndef KHTTPSD_PARAMETERS_H
#define KHTTPSD_PARAMETERS_H

//#include <linux/module.h>
#include <linux/moduleparam.h>

#include "server.h"

struct parameters {
    unsigned int port_;
    enum server_states server_state_;
    unsigned int number_of_dispatchers_;
};

extern struct parameters parameters_;

extern struct kernel_param_ops port_operations_;

extern struct kernel_param_ops number_of_dispatchers_operations_;

#endif// KHTTPSD_PARAMETERS_H

