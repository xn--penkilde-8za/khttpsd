#ifndef KHTTPSD_ELF_H_INCLUDED
#define KHTTPSD_ELF_H_INCLUDED

int load_elf_library(struct file* $file_);

void* find_symbol(const char* $name_);

void unload_elf_library(void);

#endif// KHTTPSD_ELF_H_INCLUDED
