/*
 * This file is a minor modification on the reputable picohttpparser.h
 * All credits goes to Kazuho Oku, Tokuhiro Matsuno, Daisuke Murase, Shigeo Mitsunari
 */

#ifndef KHTTPSD_HTTP_PARSER_H_INCLUDED
#define KHTTPSD_HTTP_PARSER_H_INCLUDED

#include <linux/types.h>

/* contains name and value of a header (name == NULL if is a continuing line of a multiline header */
struct http_header {
  const char* name;
  size_t name_len;
  const char* value;
  size_t value_len;
};

/* returns number of bytes consumed if successful, -2 if request is partial, -1 if failed */
int parse_http_request(
    const char* buf,
    size_t len,
    const char** method,
    size_t* method_len,
    const char** path,
    size_t* path_len,
    struct http_header* headers,
    size_t* num_headers,
    size_t last_len);

/* ditto */
int phr_parse_response(
    const char* _buf,
    size_t len,
    int* minor_version,
    int* status,
    const char** msg,
    size_t* msg_len,
    struct http_header* headers,
    size_t* num_headers,
    size_t last_len);

/* ditto */
int phr_parse_headers(const char* buf, size_t len, struct http_header* headers, size_t* num_headers, size_t last_len);

#endif// KHTTPSD_HTTP_PARSER_H_INCLUDED
