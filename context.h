#ifndef KHTTPSD_CONTEXT_H_INCLUDED
#define KHTTPSD_CONTEXT_H_INCLUDED

#include <linux/tcp.h>
#include <linux/workqueue.h>

#define DEFAULT_PORT 8080

#define DEFAULT_BACKLOG 4096

#define default_number_of_dispatchers 1

#define RECV_BUFFER_SIZE 4096

#define WORK_BUFFER_SIZE (unsigned long) (6 * 4096)

#define ARRANGMENT_SIZE sizeof(struct arrangement)

#define ARRANGMENT_BUFFER_SIZE (WORK_BUFFER_SIZE + ARRANGMENT_SIZE)

#define HTTP_GET "GET"

//in bytes
#define MAX_HTTP_HEADER_SIZE 16384

struct arrangement {
  struct work_struct work_;
  struct socket* $socket_;
  void* $work_buffer_;
};

#endif// KHTTPSD_CONTEXT_H_INCLUDED
