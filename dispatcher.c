#include <linux/kthread.h>
#include <linux/delay.h>

#include "context.h"
#include "dispatcher.h"
#include "worker.h"
#include "net.h"

int dispatcher(void* data) {
  struct dispatcher_task* $dispatcher_task_ = data;


  //TODO: test with an unbound workqueue, that may in fact
  struct workqueue_struct* $khttpsd_workqueue_ = alloc_workqueue(
    "khttpsd-workqueue-%d",
    WQ_HIGHPRI | WQ_POWER_EFFICIENT,
    512, // drag this out into a paramter
    $dispatcher_task_->id_
  );

  int accept_flags = 0;

  struct socket* $worker_socket_;

  int count_ = 0;

  int max_count_ = WQ_UNBOUND_MAX_ACTIVE * 3;

  // what if one request need more space?
  // must check this size - it is probably much larger than needed
  // max 4mb
  void* $memory_pool_ = vmalloc(WQ_UNBOUND_MAX_ACTIVE * ARRANGMENT_BUFFER_SIZE * 3);

  if (!$memory_pool_) {
    pr_err("vmalloc of size %ld failed", WQ_UNBOUND_MAX_ACTIVE * ARRANGMENT_BUFFER_SIZE);

    //record_ = -1;

    //goto RETURN;
  }

  while (!kthread_should_stop()) {
    set_current_state(TASK_INTERRUPTIBLE);

    int record_ = kernel_accept(khttpsd_net_.$socket_, &$worker_socket_, accept_flags); // blocking

    if (record_ < 0) {
      if (signal_pending(current)) {
        pr_err("broke out");
        break;
      }

      continue;
    }

    // it's on!

    // remove this later
    // bzero($preallocated_buffer_ + (count_ * ARRANGMENT_BUFFER_SIZE), ARRANGMENT_BUFFER_SIZE);
    struct arrangement* $arrangement_ = $memory_pool_ + (count_ * ARRANGMENT_BUFFER_SIZE);

    $arrangement_->$socket_ = $worker_socket_;

    $arrangement_->$work_buffer_ = $arrangement_ + ARRANGMENT_SIZE;

    INIT_WORK(&$arrangement_->work_, worker);

    int socket_cpu_id_ = READ_ONCE($worker_socket_->sk->sk_incoming_cpu);

    queue_work_on(socket_cpu_id_, $khttpsd_workqueue_, &$arrangement_->work_);

    //queue_work($khttpsd_workqueue_, &$arrangement_->work_);

    // NOTE
    // the designated worker is responsible for closing the $worker_socket_

    count_++;

    if (count_ > max_count_) {
      count_ = 0;
    }
  }

  destroy_workqueue($khttpsd_workqueue_); // All work currently pending will be done first.

  vfree($memory_pool_);

  printk("dispatcher-%d is done", $dispatcher_task_->id_);

  return 0;
}
