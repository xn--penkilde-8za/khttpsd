#ifndef KHTTPSD_NET_H
#define KHTTPSD_NET_H

#include <net/sock.h>
#include <net/tcp.h>

struct khttpsd_net {
  struct socket* $socket_;
};

extern struct khttpsd_net khttpsd_net_;

int socket_make(void);

void socket_destroy(void);

#endif// KHTTPSD_NET_H


