#ifndef KHTTPSD_SERVER_H
#define KHTTPSD_SERVER_H

enum server_states {
  server_start = 0,
  server_stop
};

struct server {
  unsigned int* $port_;
};

extern struct server server_;

int server_make(void);

void server_destroy(void);

#endif// KHTTPSD_SERVER_H
