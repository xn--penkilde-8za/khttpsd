#include "context.h"
#include "elf.h"
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/vmalloc.h>

#include "template_instantiator.h"

#define CRLF "\r\n"

#define HTTP_RESPONSE_200_DUMMY                             \
  ""                                                        \
  "HTTP/1.1 200 OK" CRLF "Server: " KBUILD_MODNAME CRLF     \
  "Content-Type: text/plain" CRLF "Content-Length: 12" CRLF \
  "Connection: Close" CRLF CRLF "Hello World!" CRLF

#define HTTP_RESPONSE_200_KEEPALIVE_DUMMY                                 \
  ""                                                                      \
  "HTTP/1.1 200 OK" CRLF "Server: " KBUILD_MODNAME CRLF                   \
  "Content-Type: text/html; charset=UTF-8" CRLF "Content-Length: 12" CRLF \
  "Connection: Keep-Alive" CRLF CRLF "Hello World!" CRLF

#define HTTP_RESPONSE_200_KEEPALIVE_DUMMY2                  \
  ""                                                        \
  "HTTP/1.1 200 OK" CRLF "Server: " KBUILD_MODNAME CRLF     \
  "Content-Type: text/html" CRLF "Content-Length: 194" CRLF \
  "Connection: Keep-Alive" CRLF CRLF

char* $template_begin_symbol_ = "{";

char* $template_end_symbol_ = "}";

char* $template_terminate_symbol_ = "\0";

char* substring(
  char* destination_,
  char* source_,
  int position,
  int length
){
  while (length > 0) {
    *destination_ = *(source_ + position);

    destination_++;

    source_++;

    length--;
  }

  *destination_ = *$template_terminate_symbol_;

  return destination_;
}

void* instantiate_template(
  struct file* $template_file_,
  char* $template_buffer_,
  char* $temploid_buffer_,
  loff_t file_size_
){
  ssize_t bytes_ = kernel_read($template_file_, $template_buffer_, file_size_, 0);

  char* $template_buffer_start_ = $template_buffer_;

  int count_ = 0;

  bool is_code_ = false;

  char* $http_header_ = HTTP_RESPONSE_200_KEEPALIVE_DUMMY2;

  int http_header_size_ = strlen($http_header_);

  memcpy($temploid_buffer_, $http_header_, http_header_size_);

  int $temploid_position_ = http_header_size_;

  int $template_position_ = 0;

  for (count_ = 0; count_ < bytes_; count_++, $template_buffer_++) {
    if (is_code_) {
      int first_char_position_ = 0;

      int last_char_position_ = 0;

      while (*$template_buffer_ != *$template_end_symbol_) {
        if (first_char_position_ == 0 && !isspace(*$template_buffer_)) {
          first_char_position_ = count_;
        } else if (first_char_position_ > 0 && *$template_buffer_ == ';') {
          last_char_position_ = count_;

          char destination[last_char_position_ - first_char_position_];

          substring(destination, $template_buffer_start_, first_char_position_, last_char_position_ - first_char_position_ - 2);

          char* (*$function_)(void);

          $function_ = find_symbol(destination);

          if ($function_ == NULL) {
            printk("symbol is NULL");
          } else {
            int foo_size = strlen((*$function_)());

            memmove($temploid_buffer_ + $temploid_position_, (*$function_)(), foo_size);

            $temploid_position_ = $temploid_position_ + foo_size;

            first_char_position_ = 0;

            last_char_position_ = 0;
          }

          $template_buffer_++;

          count_++;
        }

        $template_buffer_++;

        count_++;
      }

      is_code_ = false;

      $template_buffer_++;

      count_++;

      $template_position_ = count_;
    }


    if (*$template_buffer_ == *$template_begin_symbol_) {
      memmove($temploid_buffer_ + $temploid_position_, $template_buffer_start_ + $template_position_, count_ - $template_position_);

      $temploid_position_ = $temploid_position_ + (count_ - $template_position_);

      is_code_ = true;
    }
  }

  memmove($temploid_buffer_ + $temploid_position_, $template_buffer_start_ + $template_position_, count_ - 1 - $template_position_);

  $temploid_position_ = $temploid_position_ + (count_ - $template_position_);

  return $temploid_buffer_;
}
