#include <linux/kthread.h>
#include <linux/slab.h>

#include "parameters.h"

#include "dispatcher.h"

#include "server.h"

#include "net.h"

static struct dispatcher_task* $dispatcher_tasks_[128];

static int dispatcher_tasks_count_;

struct server server_ = {
  .$port_ = &parameters_.port_,
};

int server_make(void) {
  int record_ = 0;

  socket_make();

  for (dispatcher_tasks_count_ = 0; dispatcher_tasks_count_ < parameters_.number_of_dispatchers_; dispatcher_tasks_count_++) {
    $dispatcher_tasks_[dispatcher_tasks_count_] = kmalloc(sizeof(struct dispatcher_task), GFP_KERNEL);

    $dispatcher_tasks_[dispatcher_tasks_count_]->id_ = dispatcher_tasks_count_;

    //$dispatcher_contexts_[count_]->$socket_ = $socket_;

    // kthread_create_on_cpu
    $dispatcher_tasks_[dispatcher_tasks_count_]->$thread_ = kthread_run(
      dispatcher,
      $dispatcher_tasks_[dispatcher_tasks_count_],
      "khttpsd-dispatcher-%d",
      dispatcher_tasks_count_);

    printk("Started khttpsd-dispatcher-%d", dispatcher_tasks_count_);
  }

  return record_;
}

void server_destroy(void) {
  // stop accepting calls to socket
  for (int count_ = 0; count_ < dispatcher_tasks_count_; count_++) {
    kthread_stop($dispatcher_tasks_[count_]->$thread_); // stops the dispatcher and frees the task_struct

    kfree($dispatcher_tasks_[count_]);

    printk("Stopped khttpsd-dispatcher-%d", count_);

  }

  socket_destroy();
}
