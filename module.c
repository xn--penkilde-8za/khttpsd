#include <linux/err.h>
#include <linux/in.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/sockios.h>
#include <linux/string.h>
#include <linux/tcp.h>
#include <linux/version.h>
#include <linux/vmalloc.h>
#include <net/sock.h>
#include <net/tcp.h>

#include "parameters.h"
#include "server.h"

//static void* $memory_pool_;



//static struct dispatcher_context* $dispatcher_contexts_[128];

//static struct task_struct* $kernel_threads_[128];

//struct kobject* khttpsd_sysfs;

// module data

MODULE_LICENSE("GPL");

MODULE_AUTHOR("Rino André Johnsen <rino@åpenkilde.org>");

MODULE_DESCRIPTION("khttpsd is a HTTPS-daemon (webserver) that runs from within the Linux-kernel as a module");

MODULE_VERSION("0.1");

// parameters

module_param_cb(
  port,
  &port_operations_,
  &parameters_.port_,
  0644
);

MODULE_PARM_DESC(
  port,
  " The service endpoint identifier. Identifiers from 0 to 1024 are reserved for privileged services and designated as well-known ports."
  " | type=number | pattern=[0-9]{1,5} | range={0...65536} |"
);

module_param_cb(
  number_of_dispatchers,
  &number_of_dispatchers_operations_,
  &parameters_.number_of_dispatchers_,
  0644
);

MODULE_PARM_DESC(
  number_of_dispatchers,
  " Max MTU value."
);

static int __init khttpsd_initialize(void) {
  int record_ = 0;

  /*Creating a directory in /sys/kernel/ */
  //khttpsd_sysfs = kobject_create_and_add("khttpsd", fs_kobj); // TODO: find right place for this


  /*for (int index_ = 1; index_ < number_of_dispatchers; index_++) {
    $dispatcher_contexts_[index_] = kmalloc(sizeof(struct dispatcher_context), GFP_KERNEL);

    $dispatcher_contexts_[index_]->id_ = index_;

    $dispatcher_contexts_[index_]->$socket_ = $socket_;

    $kernel_threads_[index_] = kthread_run(dispatcher, $dispatcher_contexts_[index_], "khttpsd-dispatcher-%d", index_);
  }*/

  /*if (IS_ERR($kernel_thread_)) {
    pr_err("Can't create thread");

    record_ = -1;

    goto RETURN;
  }*/

  //load elf file here
  /*
  char* file_path_ = "/home/rino/Projects/kc/build/libkc.so";

  struct file* file_ = filp_open(file_path_, O_RDWR, 0);

  if (IS_ERR(file_)) {
    pr_err("Can't open file");

    record_ = -1;

    goto RETURN;
  }

  record_ = load_elf_library(file_);

  filp_close(file_, NULL);

  if (record_ < 0) {
    pr_err("Can't load library");

    goto RETURN;
  }
  */


  //if (parameters_.server_state_ == server_start) {
    server_make();
  //}

  pr_info("Loaded khttpsd");
RETURN:
  return record_;
}

static void __exit khttpsd_exit(void) {
  //flush_workqueue($khttpsd_workqueue_);

  //destroy_workqueue($khttpsd_workqueue_);



  //kthread_stop($kernel_thread_);

  //kthread_stop($kernel_thread2_);

  //socket_exit();

  //unload_elf_library();

  //vfree($memory_pool_);

  server_destroy();

  pr_info("Unloaded khttpsd");
}

module_init(
  khttpsd_initialize
);

module_exit(
  khttpsd_exit
);
