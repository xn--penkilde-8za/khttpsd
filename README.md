# khttpsd

khttpsd is a HTTPS-daemon (webserver) for Linux. khttpsd is different from other webservers in that it runs from within the Linux-kernel as a module.
The goal for khttpsd is to use the least amount of energy to process a request.


## 1. Introduction

In the recent years, it has become increasingly popular for various software, and especially webservers, to experiment with [io_uring](https://en.wikipedia.org/wiki/Io_uring) and [eBPF](https://en.wikipedia.org/wiki/Berkeley_Packet_Filter) to execute code in kernel-space instead of user-space. The performance hit from jumping between user-space and kernel-space has increased drastically with the Meltdown and Spectre vulnerabilities.

[ÅPENKILDE](https://www.åpenkilde.org) has also experimented with io_uring and eBPF - and from the result, it seems reasonable to skip both technologies and instead implement performance sensitive software in the Linux-kernel as a module. To showcase the benefit from this, ÅPENKILDE decided to implement an HTTPS webserver as a Linux-kernel module. Such a project also falls in line with ÅPENKILDEs philosophy in regard to development of software and is intended to serve www.åpenkilde.org when it is ready for production use.

## 2. Status

### 2.1 Stability

khttpsd is currently _not_ stable enough for production use.

### 2.2 Features

khttpsd has currently the following features:

- Asynchronous execution of each equest
- On-demand concurrency
- Static and dynamic file-serving
- System suspend and wake-up on arrival of new data on socket

## 3. Usage

### 3.1 Static-only file-serving

- DEFAULT_PORT is set to 8082. Edit "main.c" to change the port.
- Build the module (`make`)
- Place static files in '/var/www/khttpsd/' (http://localhost:8082/my-folder/ -> look for index.html in /var/www/khttpsd/my-folder)
- Load the module with insmod" (`sudo insmod khttpsd.ko`)

### 3.2 Dynamic file-serving

- DEFAULT_PORT is set to 8082. Edit "main.c" to change the port.
- Run "make"
- Create and build a ".so" library[^1]
- Create index.template files
- Place ".so" library files in '/var/www/khttpsd/'
- Place static files in '/var/www/khttpsd/' (http://localhost:8082/my-folder/ -> look for index.template (looks for index.html if no template is found) in /var/www/khttpsd/my-folder)
- Load the module with insmod" (`sudo insmod khttpsd.ko`)

[^1]: Any ".so" library that is intended to be used with khttpsd must be kernel-c only. That means the c-code in the library can only consist of code that does not use any syscalls.

## 4. Performance

khttpsd perform very well in benchmarks. It has mostly been tested with static file-serving to tune it well.[^2]

The benchmarks performed has been in comparison with:
- [Drogon](https://github.com/drogonframework/drogon) - among the fastest webservers in the world[^3].
- [khttpd](https://github.com/sysprog21/khttpd) - another webserver that it runs from within the Linux-kernel as a module.

khttpsd aim to be on par with Drogon and better than khttpd in terms of performance.

| Webserver     | Requests/sec  |
| ------------- |:--------------|
| khttpsd       | 1418853       |
| Drogon        | 1202478[^4]   |
| khttpd        | 1175808       |

While khttpsd is outperforming Drogon by a 16.5% difference, here is still a lot of optimizing left to do for khttpsd, as a difference of 33% should be achievable from running a webserver in kernel-space[^7]. In other words, a perfect port of Drogon to kernel-space would mean that it would score somewhere around 1678853.

[^2]: All testing so far has been done on loopback, in other words, `khttpsd` and `wrk` has executed on the same machine. The benchmarks has been runned on an 16 core Intel i7-11700 with 32gb ram at 2933MHz.
[^3]: [Web Framework Benchmarks](https://www.techempower.com/benchmarks/)
[^4]: Drogon has been configured to use 16 threads.
[^7]: [Speculative Execution Mitigations Benchmark](https://talawah.io/blog/linux-kernel-vs-dpdk-http-performance-showdown/#speculative-execution-mitigations)

## 5. Energy consumption

khttpsd is designed to use as the least amount of energy to a) serve a static file and b) serve a dynamic file.

The design is based on fact that there is some correlation between energy consumption and the data being processed[^5]. In other words, the less data that is being used the less energy is used.
There are other factors as well - and currently the design is based on theoretical knowledge. ÅPENKILDE has experimented with [Intel RAPL](https://01.org/blogs/2014/running-average-power-limit-%E2%80%93-rapl) to measure the energy consumption of _one_ request, but have not succeded in getting reliable data from it. 

khttpsd has the option to put the entire system in sleep-mode after a request is fulfilled and wake-up the system on arrival of new data on socket, so that the system overall uses the least amount of energy[^6].

[^5]: [Running Average Power Limit Energy Reporting](https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/running-average-power-limit-energy-reporting.html)

[^6]: This is only possible on system where the network interface controller (NIC) draws power diectly from the power supply and not from the system interface. This feature has been tested on a [Dell Edge Gateway 3002](https://www.dell.com/en-us/work/shop/cty/new-dell-edge-gateway-3002/spd/dell-edge-gateway-3002) that has the NIC on it's own power supply.

## 6. Future

### 6.1 TLS

khttpsd has yet to implement the TLS layer. This is a top priority.

~~The plan is to shoehorn in the [Tempesta TLS](https://github.com/tempesta-tech/tempesta/tree/master/tls) kernel-library, which implements fast TLS handshakes and uses the native Linux crypto API for the symmetric cryptography algorithms.~~

After some investigation, it turn out that the maintainers of NFS has portet the [Tempesta TLS](https://github.com/tempesta-tech/tempesta/tree/master/tls) kernel-library and is currently [awaiting upstream approval](https://git.kernel.org/pub/scm/linux/kernel/git/cel/linux.git/log/?h=topic-rpc-with-tls-upcall). khttpsd will use the NFS port when it is merged.

### 6.2 epoll

khttpsd currently relies on [Concurrency Managed Workqueue](https://www.kernel.org/doc/html/latest/core-api/workqueue.html), which is excellent, but there is minor overhead from switching between ktreads. epoll can be used to bypass such overheads by pinning each epoll-loop on spesific cpus. Drogon uses the epoll model.

### 6.3 Smarter elf-loader

The elf-loader used by khttpsd is very simplistic. It should be extended to load dependencies.

