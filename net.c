#include "parameters.h"

#include "net.h"

#include "context.h"

struct khttpsd_net khttpsd_net_ = {
  .$socket_ = NULL,
};

int socket_make(void) {
  int record_ = sock_create(PF_INET, SOCK_STREAM, IPPROTO_TCP, &khttpsd_net_.$socket_);

  if (record_ < 0) {
    pr_err("sock_create() failure, error=%d\n", record_);

    goto RETURN;
  }

  sock_set_reuseaddr(khttpsd_net_.$socket_->sk);

  sock_set_reuseport(khttpsd_net_.$socket_->sk);

  tcp_sock_set_nodelay(khttpsd_net_.$socket_->sk);

  tcp_sock_set_quickack(khttpsd_net_.$socket_->sk, 2);

  //enable_busy_poll(khttpsd_net_.$socket_);

  int one = 1;

  record_ = tcp_setsockopt(khttpsd_net_.$socket_->sk, SOL_TCP, TCP_DEFER_ACCEPT, KERNEL_SOCKPTR(&one), sizeof(one));

  if (record_ < 0) {
    pr_err("TCP_DEFER_ACCEPT failure, error=%d", record_);

    goto SOCKET_UNDO;
  }

  struct sockaddr_in socket_address = {
      .sin_family = AF_INET,
      .sin_addr.s_addr = htonl(INADDR_ANY),
      .sin_port = htons(parameters_.port_)
  };

  // any number higher than 0
  sock_set_priority(khttpsd_net_.$socket_->sk, 0664);

  khttpsd_net_.$socket_->sk->sk_allocation = GFP_ATOMIC;

  record_ = kernel_bind(khttpsd_net_.$socket_, (struct sockaddr*) &socket_address, sizeof(socket_address));

  if (record_ < 0) {
    pr_err("kernel_bind() failure, err=%d", record_);

    goto SOCKET_UNDO;
  }

  record_ = kernel_listen(khttpsd_net_.$socket_, DEFAULT_BACKLOG);

  if (record_ < 0) {
    pr_err("kernel_listen() failure, error=%d", record_);

    goto SOCKET_UNDO;
  }

  return 0;

SOCKET_UNDO:
  sock_release(khttpsd_net_.$socket_);
RETURN:
  return record_;
}

void socket_destroy(void) {
  kernel_sock_shutdown(khttpsd_net_.$socket_, SHUT_RDWR);

  sock_release(khttpsd_net_.$socket_);

  khttpsd_net_.$socket_ = NULL;
}

void enable_busy_poll(struct socket* $socket_) {
  int sock_opt;

  sock_opt = 1;
  if (sock_setsockopt($socket_, SOL_SOCKET, SO_PREFER_BUSY_POLL, KERNEL_SOCKPTR(&sock_opt), sizeof(sock_opt)) < 0) {
    //exit_with_error(errno);
    printk("failed to set SO_PREFER_BUSY_POLL");
  }

  sock_opt = 50;
  if (sock_setsockopt($socket_, SOL_SOCKET, SO_BUSY_POLL, KERNEL_SOCKPTR(&sock_opt), sizeof(sock_opt)) < 0) {
    //exit_with_error(errno);
    printk("failed to set SO_BUSY_POLL");
  }

  sock_opt = 64;
  if (sock_setsockopt($socket_, SOL_SOCKET, SO_BUSY_POLL_BUDGET, KERNEL_SOCKPTR(&sock_opt), sizeof(sock_opt)) < 0) {
    //exit_with_error(errno);
    printk("failed to set SO_BUSY_POLL_BUDGET");
  }
}
