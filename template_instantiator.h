#ifndef KHTTPSD_TEMPLATE_INSTANTIATOR_H_INCLUDED
#define KHTTPSD_TEMPLATE_INSTANTIATOR_H_INCLUDED

#include <linux/fs.h>

void* instantiate_template(struct file* $template_file_, char* $template_buffer_, char* $temploid_buffer_, loff_t file_size_);

#endif// KHTTPSD_TEMPLATE_INSTANTIATOR_H_INCLUDED
