KDIR=/lib/modules/$(shell uname -r)/build

obj-m += khttpsd.o
khttpsd-objs := module.o parameters.o net.o server.o dispatcher.o worker.o http_parser.o

#worker.o elf.o http_parser.o template_instantiator.o http_response_composer.o

ccflags-y += -Ofast
ccflags-y += -std=gnu11 -Wno-declaration-after-statement -funroll-loops

all:
	make -C $(KDIR) M=$(PWD) modules

clean:
	make -C $(KDIR) M=$(PWD) clean
