#include <linux/elf.h>
#include <linux/elfcore.h>
#include <linux/export.h>
#include <linux/filter.h>
#include <linux/in.h>
#include <linux/io.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/sizes.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>

#include "elf.h"

#if ELF_CLASS == ELFCLASS32
  #define elf_sym elf32_sym
#else
  #define elf_sym elf64_sym
#endif

static char* $file_buffer_;

static char* $code_buffer_;

static struct elfhdr* $elf_header_;

static struct elf_shdr* $elf_section_header_;

int load_elf_library(struct file* $file_) {
  loff_t file_size_ = i_size_read(file_inode($file_));

  $file_buffer_ = kzalloc(file_size_, GFP_KERNEL);

  int error_code_ = kernel_read($file_, $file_buffer_, file_size_, 0);

  $code_buffer_ = kzalloc(file_size_, GFP_KERNEL);

  $elf_header_ = (struct elfhdr*) $file_buffer_;

  struct elf_phdr* $elf_program_header_ = (struct elf_phdr*) ($file_buffer_ + $elf_header_->e_phoff);

  $elf_section_header_ = (struct elf_shdr*) ($file_buffer_ + $elf_header_->e_shoff);

  for (int index_ = 0; index_ < $elf_header_->e_phnum; ++index_) {
    if ($elf_program_header_[index_].p_type != PT_LOAD) {
      continue;
    }

    if (!$elf_program_header_[index_].p_filesz) {
      continue;
    }

    if ($elf_program_header_[index_].p_filesz > $elf_program_header_[index_].p_memsz) {
      return -1;
    }

    char* $start_ = $file_buffer_ + $elf_program_header_[index_].p_offset;

    char* $taddr_ = $elf_program_header_[index_].p_vaddr + $code_buffer_;

    memmove($taddr_, $start_, $elf_program_header_[index_].p_filesz);
  }

  return 0;
}

void* find_symbol(const char* $name_) {
  for (int index_ = 0; index_ < $elf_header_->e_shnum; ++index_) {
    if ($elf_section_header_[index_].sh_type == SHT_DYNSYM) {
      char* $strings_ = $file_buffer_ + $elf_section_header_[$elf_section_header_[index_].sh_link].sh_offset;

      struct elf_shdr* lol = $elf_section_header_ + index_;

      struct elf_sym* $symbols_ = (struct elf_sym*) ($file_buffer_ + lol->sh_offset);

      for (int index_ = 0; index_ < lol->sh_size / sizeof(struct elf_sym); index_ += 1) {
        if (strcmp($name_, $strings_ + $symbols_[index_].st_name) == 0) {
          return $code_buffer_ + $symbols_[index_].st_value;
        }
      }

      break;
    }
  }

  return NULL;
}

void unload_elf_library(void) {
  kfree($file_buffer_);

  kfree($code_buffer_);
}
