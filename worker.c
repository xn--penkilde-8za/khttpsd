#include <linux/hashtable.h>
#include <linux/socket.h>
#include <linux/suspend.h>
#include <linux/tcp.h>
#include <linux/skbuff.h>
#include <linux/skmsg.h>
#include <linux/socket.h>
#include <net/tcp.h>
#include <net/busy_poll.h>

#include "context.h"
#include "http_parser.h"
#include "http_response_composer.h"
#include "worker.h"

u8* page_address$ = NULL;

int read_socket(
  struct sock* socket$,
  struct kvec* vector$,
  size_t size_
){
  /*if (
    sk_can_busy_loop(socket$) &&
    skb_queue_empty_lockless(&socket$->sk_receive_queue) &&
    socket$->sk_state == TCP_ESTABLISHED
  ) {
    //printk("Eanbled busy_poll");

    sk_busy_loop(socket$, MSG_DONTWAIT);
  }*/

  lock_sock(socket$);

  struct tcp_sock* tcp_socket_ = tcp_sk(socket$);

  int copied = 0;

  unsigned long used;

  int target = sock_rcvlowat(socket$, MSG_WAITALL, size_);

  struct sk_buff *socket_buffer$, *last;

  u32* seq = &tcp_socket_->copied_seq;

  long timeout_ = sock_rcvtimeo(socket$, 0 & MSG_DONTWAIT);

  do {
    u32 offset_;

    last = skb_peek_tail(&socket$->sk_receive_queue);

    /*skb_queue_walk(&socket$->sk_receive_queue, socket_buffer$) {
      last = socket_buffer$;

      offset_ = *seq - TCP_SKB_CB(socket_buffer$)->seq;

      if (offset_ < socket_buffer$->len) {
        //pr_info("found_ok_skb");

        goto found_ok_skb;
      }

      if (TCP_SKB_CB(socket_buffer$)->tcp_flags & TCPHDR_FIN) {
        //pr_info("found_fin_ok");

        goto found_fin_ok;
      }
    }*/

    while ((socket_buffer$ = tcp_recv_skb(socket$, *seq, &offset_)) != NULL) {
      last = socket_buffer$;

      offset_ = *seq - TCP_SKB_CB(socket_buffer$)->seq;

      if (offset_ < socket_buffer$->len) {
        //pr_info("found_ok_skb");

        goto found_ok_skb;
      }

      if (TCP_SKB_CB(socket_buffer$)->tcp_flags & TCPHDR_FIN) {
        //pr_info("found_fin_ok");

        goto found_fin_ok;
      }
    }

    if (copied >= target && !READ_ONCE(socket$->sk_backlog.tail)) {
      //pr_info("Beep");

      break;
    }

    if (copied) {
      if (!timeout_ || socket$->sk_err || socket$->sk_state == TCP_CLOSE || (socket$->sk_shutdown & RCV_SHUTDOWN) || signal_pending(current)) {
         //pr_info("Boop");


        break;
      }
    } else {
      if (sock_flag(socket$, SOCK_DONE)) {
        //pr_info("SOCK_DONE");

        break;
      }

      if (socket$->sk_err) {
        //pr_info("SOCK_error");
        copied = sock_error(socket$);

        break;
      }

      if (socket$->sk_shutdown & RCV_SHUTDOWN) {
        //pr_info("RCV_SHUTDOWN");

        break;
      }

      if (socket$->sk_state == TCP_CLOSE) {
        //pr_info("SOCK_CLOSE");
        copied = -ENOTCONN;

        break;
      }

      if (!timeout_) {
        //this means that we actually timed out
        //pr_info("SOCK_agiain");
        copied = 0;

        break;
      }

      if (signal_pending(current)) {
        //pr_info("SOCK_timeout");

        copied = sock_intr_errno(timeout_);

        break;
      }
    }

    if (copied >= target) {
      pr_info("Flushing");
      /* Do not sleep, just process backlog. */
      __sk_flush_backlog(socket$);
    } else {
      //tcp_cleanup_rbuf(socket$, copied);

      //tcp_read_done(socket$, copied);

      /*release_sock(socket$);

      sk_busy_loop(socket$, MSG_DONTWAIT);

      printk("done");

      lock_sock(socket$);*/

      if (skb_queue_empty_lockless(&socket$->sk_receive_queue)) {
        //printk("sleeping");
        sk_wait_data(socket$, &timeout_, last);
      } else {
        printk("found data by pooling");
      }

      /*if (
        sk_can_busy_loop(socket$) &&
        skb_queue_empty_lockless(&socket$->sk_receive_queue) &&
        socket$->sk_state == TCP_ESTABLISHED
      ) {
        printk("polling");
        sk_busy_loop(socket$, MSG_DONTWAIT);
      } else {
        pr_info("Sleeping");

      }*/

      /*sk_set_bit(SOCKWQ_ASYNC_WAITDATA, socket$);
      release_sock(socket$);

      lock_sock(socket$);

      sk_clear_bit(SOCKWQ_ASYNC_WAITDATA, socket$);*/
    }

    continue;
  found_ok_skb:
    used = socket_buffer$->len - offset_;

    if (size_ < used) {
      used = size_;
    }

    //struct kvec* vector$ = read_descriptor$->arg.data;

    for (int i = 0; i < skb_shinfo(socket_buffer$)->nr_frags; i++) {
      //pr_info("page: %d", i);

      const skb_frag_t* data_fragment$ = &skb_shinfo(socket_buffer$)->frags[i];

      struct page* page$ = skb_frag_page(data_fragment$);

      page_address$ = page_address(page$) + data_fragment$->bv_offset;
    }

    /*struct sk_buff* frag_iter;

    skb_walk_frags(socket_buffer$, frag_iter) {
      for (int i = 0; i < skb_shinfo(frag_iter)->nr_frags; i++) {
        pr_info("page: %d", i);

        const skb_frag_t* data_fragment$ = &skb_shinfo(frag_iter)->frags[i];

        struct page* page$ = skb_frag_page(data_fragment$);

        page_address$ = page_address(page$) + data_fragment$->bv_offset;
      }
    }*/

    // check if this works
    //page_address$ = socket_buffer$->data;

    used = socket_buffer$->len - offset_;

    WRITE_ONCE(*seq, *seq + used);
    copied += used;
    size_ -= used;

    if (TCP_SKB_CB(socket_buffer$)->tcp_flags & TCPHDR_FIN) {
      goto found_fin_ok;
    }

    //continue;
  found_fin_ok:
    WRITE_ONCE(*seq, *seq + 1);

    if (!(0 & MSG_PEEK)) {
      //pr_info("found_fin_ok2");

      //__skb_unlink(socket_buffer$, &socket$->sk_receive_queue);

      //tcp_read_done(socket$, copied);

      //copied = -EAGAIN;
    }

    break;
  } while (size_ > 0);

  /* Clean up data we have read: This will do ACK frames. */
  //tcp_cleanup_rbuf(socket$, copied);

  release_sock(socket$);

  return copied;
}




void worker(struct work_struct* work$) {
  struct http_header http_headers_[32];

  const char* $method_,* $path_;

  size_t buffer_length_ = 0, previous_buffer_length_ = 0, method_length_, path_length_, headers_count_;

  headers_count_ = sizeof(http_headers_) / sizeof(http_headers_[0]);

  struct arrangement* $arrangement_ = container_of(work$, struct arrangement, work_);

  // treating everything as keep-alive, so a timeout closes the connection.
  $arrangement_->$socket_->sk->sk_rcvtimeo = 10 * HZ;// 10 second

  void* $file_buffer_ = $arrangement_->$work_buffer_ + 4096;

  void* $response_buffer_ = $arrangement_->$work_buffer_ + 8192;

  //printk("worker cpu: %d", smp_processor_id());

  do {
    // look into recv hints (https://git.kernel.dk/cgit/linux-block/log/?h=io_uring-flags2)

    //printk("worker-read cpu: %d", smp_processor_id());

    int tt = PAGE_SIZE;
    // pages are 4kb
    // struct kvec* vector =
    struct kvec send_message_data_[MAX_HTTP_HEADER_SIZE / PAGE_SIZE];

    //pr_info("Reading socket");
    int record_ = read_socket(
      $arrangement_->$socket_->sk,
      NULL,
      1000
    );

    int ddd = record_;

    //pr_err("record_ shows: %d", record_);

    if (record_ <= 0 || page_address$ == NULL) {
      if (sock_flag($arrangement_->$socket_->sk, SOCK_DONE)) {
      //pr_info("SOCK_DONE");
      // if we get here

      //goto out;
    }

    if ($arrangement_->$socket_->sk->sk_err) {
      record_ = sock_error($arrangement_->$socket_->sk);

      //pr_info("ERROR");

      //goto out;
    }

    if ($arrangement_->$socket_->sk->sk_shutdown & RCV_SHUTDOWN) {
      //pr_info("SHUTDOWN");

      //goto out;
    }

    if ($arrangement_->$socket_->sk->sk_state == TCP_CLOSE) {
      record_ = -ENOTCONN;

      //pr_info("CLOSE");

      //goto out;
    }

    /*if (!timeout_) {
      record_ = -EAGAIN;

      pr_info("timeo");

      goto out;
    }

    if (signal_pending(current)) {
      record_ = sock_intr_errno(timeout_);

      pr_info("timeo2");

      goto out;
    }*/




      if (record_ == -EAGAIN) {
        continue;
      }

      // if client closed the connection or it timed out, don't log it
      if (record_ != 0 && record_ != -ECONNRESET) {
        //pr_err("kernel_recvmsg failure: %d", record_);
      }

      goto EXIT;
    }

    previous_buffer_length_ = buffer_length_;

    buffer_length_ += record_;

    /*record_ = parse_http_request(
      page_address$,
      buffer_length_,
      &$method_,
      &method_length_,
      &$path_,
      &path_length_,
      http_headers_,
      &headers_count_,
      previous_buffer_length_
    );

    if (record_ <= 0) {
      if (record_ == 0 || record_ == -1) {// parse failure
        pr_err("parse_http_request failure");

        goto EXIT;
      }

      continue;// request is partial
    }*/

    /*int response_size_ = compose_response(
      $file_buffer_,
      $response_buffer_,
      $method_,
      method_length_,
      $path_,
      path_length_
    );*/

    // at this point, we should probably clean up data we have read: This will do ACK frames.

    //consume_skb(last_socket_buffer$);

    //tcp_read_done($arrangement_->$socket_->sk, ddd);

    int done_ = 0;

    do {
      struct msghdr send_message_ = {.msg_flags = MSG_DONTWAIT | MSG_NOSIGNAL};

      struct kvec send_message_data_[] = {
        {.iov_base = HTTP_RESPONSE_HELLO_WORLD, .iov_len = HTTP_RESPONSE_HELLO_WORLD_SIZE}
      };

      iov_iter_kvec(&send_message_.msg_iter, READ, send_message_data_, 1, HTTP_RESPONSE_HELLO_WORLD_SIZE);

      record_ = tcp_sendmsg(
        $arrangement_->$socket_->sk,
        &send_message_,
        HTTP_RESPONSE_HELLO_WORLD_SIZE
      );

      if (record_ == -EAGAIN) {
        continue;
      }

      if (record_ <= 0) {
        pr_err("write error: %d", record_);

        goto EXIT;
      }

      done_ += record_;
    } while (done_ < HTTP_RESPONSE_HELLO_WORLD_SIZE);

    buffer_length_ = 0;

    previous_buffer_length_ = 0;
  } while (!kthread_should_stop());
EXIT:
  //pr_info("EXITED");
  //pr_info("EXITED");
  //pr_info("EXITED");

  kernel_sock_shutdown($arrangement_->$socket_, SHUT_RDWR);

  sock_release($arrangement_->$socket_);

  // make a config for this
  // pm_suspend(PM_SUSPEND_MEM);
}
