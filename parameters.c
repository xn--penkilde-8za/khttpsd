#include "context.h"
#include "parameters.h"

const char* khttpsd_state_names_[] = {
  [server_start] = "Started",
  [server_stop] = "Stopped",
};

struct parameters parameters_ = {
  .port_ = DEFAULT_PORT,
  .server_state_ = server_stop,
  .number_of_dispatchers_ = default_number_of_dispatchers
};

int khttpsd_state_get_operation(char* $buffer_, const struct kernel_param* $kernel_parameter_) {
  return sprintf($buffer_, "%s\n", khttpsd_state_names_[*(enum server_states*)$kernel_parameter_->arg]);
}

int khttpsd_state_set_operation(const char* $value_, const struct kernel_param* $kernel_parameter_) {

    /* One of =[yYnN01] */

  return 0;
}

int number_of_dispatchers_set_operation(const char* $value_, const struct kernel_param* $kernel_parameter_) {
  int record_;

  record_ = param_set_uint_minmax($value_, $kernel_parameter_, 1, 256);

  if (record_) {
    return record_;
  }

  return 0;
}

int port_set_operation(const char* $value_, const struct kernel_param* $kernel_parameter_) {
  int record_;

  if (parameters_.server_state_ == server_start) {
    // maybe we sjould log some stuff here?

    return -EPERM;
  }

  record_ = param_set_uint_minmax($value_, $kernel_parameter_, 0, 65536);

  if (record_) {
    return record_;
  }

  return 0;
}

struct kernel_param_ops khttpsd_state_operations = {
  .get = khttpsd_state_get_operation,
  .set = khttpsd_state_set_operation
};

struct kernel_param_ops number_of_dispatchers_operations_ = {
  .get = param_get_uint,
  .set = number_of_dispatchers_set_operation
};

struct kernel_param_ops port_operations_ = {
  .get = param_get_uint,
  .set = port_set_operation
};
