#ifndef KHTTPSD_DISPATCHER_H
#define KHTTPSD_DISPATCHER_H

#include <linux/types.h>

struct dispatcher_task {
  int id_;
  struct socket* $socket_;
  void* $work_buffer_;
  size_t work_buffer_size_;
  struct task_struct* $thread_;
};

int dispatcher(void* data);

#endif// KHTTPSD_NET_H
