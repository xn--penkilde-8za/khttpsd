#include <linux/fs.h>
#include <linux/memory.h>
#include <linux/string.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include <linux/err.h>

#include "context.h"
#include "http_response_composer.h"
#include "template_instantiator.h"

static inline int assemble_http_response_header(
  void* $response_memory_,
  char* $http_response_header_,
  int file_size_
){
  memcpy($response_memory_, $http_response_header_, 75);

  char buffer_[128];

  int position_ = 126;

  while (file_size_ != 0) {
    buffer_[position_--] = (file_size_ % 10) + '0';

    file_size_ /= 10;
  }

  int size_ = 126 - position_;

  memcpy($response_memory_ + 75, buffer_ + position_ + 1, size_);

  memcpy($response_memory_ + 75 + size_, $http_response_header_ + 75, 28);

  return size_;
}


int compose_response(
  void* file_memory_,
  void* response_memory_,
  char* method_,
  size_t method_length,
  char* path_,
  size_t path_length
){
  // this can overflow
  char base_path_[272] = "/var/www/khttpsd";

  char* $hello_path_ = "/hello";

  char index_html[] = "index.html";

  char index_template_[] = "index.template";

  //const char* test_path = "/var/www/khttpsd/index.html";



  char full_stop_symbol = '.';

  int response_size_ = 0;

  if (strncmp(method_, HTTP_GET, method_length) != 0) {
    memcpy(response_memory_, HTTP_RESPONSE_501, HTTP_RESPONSE_501_SIZE);

    response_size_ = HTTP_RESPONSE_501_SIZE;

    goto OUT;
  }

  if (strncmp(path_, $hello_path_, path_length) == 0) {
    memcpy(response_memory_, HTTP_RESPONSE_HELLO_WORLD, HTTP_RESPONSE_HELLO_WORLD_SIZE);

    response_size_ = HTTP_RESPONSE_HELLO_WORLD_SIZE;

    goto OUT;
  }

  strncat(base_path_, path_, path_length);

  if (strchr(base_path_, full_stop_symbol) == NULL) { // template or html

  } /*else { // template or html

    strcat(base_path_, index_html_);

    struct file* $file_ = filp_open(base_path_, O_RDONLY, 0);

    if (IS_ERR($file_)) {
      $file_ = filp_open(base_path_, O_RDONLY, 0);
    }

    loff_t file_size_ = i_size_read(file_inode($file_));

    char* $http_response_header_ = HTTP_RESPONSE_200_FRAGMENT;

    instantiate_template($file_, $file_buffer_, $response_buffer_, file_size_);

    filp_close($file_, NULL);

    goto out;
  }*/

  // At this point we know it is a static file serving

  // NOTE
  // this code-path is significantly slower than the test part
  //   - this seems to be due to filp_open, which malloc data
  struct file* $file_ = filp_open(base_path_, O_RDONLY, 0);

  if (IS_ERR($file_)) {
    pr_err("filp_open(%s) failed\n", base_path_);

    goto NOT_FOUND;
  }

  long long file_size_ = i_size_read(file_inode($file_));

  if (file_size_ <= 0) {
    pr_err("i_size_read(file_inode(%s)) failed", base_path_);

    goto NOT_FOUND;
  }

  int extra_space_ = assemble_http_response_header(response_memory_, HTTP_RESPONSE_200_FRAGMENT, file_size_);

  int error_code = kernel_read(
    $file_,
    response_memory_ + (HTTP_RESPONSE_200_FRAGMENT_SIZE + extra_space_),
    file_size_,
    0
  );

  filp_close($file_, NULL);

  if (error_code <= 0) {
    pr_err("kernel_read(%s) failed", base_path_);

    goto NOT_FOUND;
  }

  response_size_ = (HTTP_RESPONSE_200_FRAGMENT_SIZE + extra_space_) + file_size_;

  goto OUT;

NOT_FOUND:
  memcpy(response_memory_, HTTP_RESPONSE_404, HTTP_RESPONSE_404_SIZE);

  response_size_ = HTTP_RESPONSE_404_SIZE;
OUT:
  return response_size_;
}
