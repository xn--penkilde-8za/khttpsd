#ifndef KHTTPSD_HTTP_RESPONSE_COMPOSER_H_INCLUDED
#define KHTTPSD_HTTP_RESPONSE_COMPOSER_H_INCLUDED

#include <linux/types.h>

#define CRLF "\r\n"

#define HTTP_RESPONSE_HELLO_WORLD                           \
  ""                                                        \
  "HTTP/1.1 200 OK" CRLF "Server: " KBUILD_MODNAME CRLF     \
  "Content-Type: text/plain" CRLF "Content-Length: 12" CRLF \
  "Connection: Keep-Alive" CRLF CRLF "Hello World!" CRLF

#define HTTP_RESPONSE_HELLO_WORLD_SIZE strlen(HTTP_RESPONSE_HELLO_WORLD)

#define HTTP_RESPONSE_200_FRAGMENT                       \
  ""                                                     \
  "HTTP/1.1 200 OK" CRLF "Server: " KBUILD_MODNAME CRLF  \
  "Content-Type: text/html" CRLF "Content-Length: " CRLF \
  "Connection: Keep-Alive" CRLF CRLF

#define HTTP_RESPONSE_200_FRAGMENT_SIZE strlen(HTTP_RESPONSE_200_FRAGMENT)

#define HTTP_RESPONSE_404                                      \
  ""                                                           \
  "HTTP/1.1 404 Not Found" CRLF "Server: " KBUILD_MODNAME CRLF \
  "Content-Type: text/plain" CRLF "Content-Length: 15" CRLF    \
  "Connection: KeepAlive" CRLF CRLF "404 Not Found" CRLF

#define HTTP_RESPONSE_404_SIZE strlen(HTTP_RESPONSE_404)

#define HTTP_RESPONSE_501                                            \
  ""                                                                 \
  "HTTP/1.1 501 Not Implemented" CRLF "Server: " KBUILD_MODNAME CRLF \
  "Content-Type: text/plain" CRLF "Content-Length: 21" CRLF          \
  "Connection: KeepAlive" CRLF CRLF "501 Not Implemented" CRLF

#define HTTP_RESPONSE_501_SIZE strlen(HTTP_RESPONSE_501)

int compose_response(
  void* file_buffer_,
  void* response_buffer_,
  char* method_address_,
  size_t method_length,
  char* path_,
  size_t path_length
);

#endif// KHTTPSD_HTTP_RESPONSE_COMPOSER_H_INCLUDED
